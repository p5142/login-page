<?php
session_start();
if(!isset($_SESSION['login'])){
    header("Location:index.php");
 }
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
    <main>
        <form method="post">
            <input type="submit" name="logout" value="DISCONNECT">
        </form>
        <p>Good job! You're connected!</p>
        <?php $end_session = !empty ($_REQUEST['logout']) ? $_REQUEST['logout'] : NULL;
 if (!empty ($end_session) === TRUE){
     session_destroy();
     header('Location: index.php');
 }?>
    </main>
    </body>
    </html>