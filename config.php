<?php 
session_start();
include("connection.php");

if(isset($_POST['loginbtn'])) {
    $sent_email = !empty ($_REQUEST["input_e-mail"]) ? $_REQUEST["input_e-mail"] : NULL;
    $sent_password = !empty ($_REQUEST["input_password"]) ? $_REQUEST["input_password"] : NULL;
    // var_dump($sent_password . $sent_email);
    if(!empty ($sent_email && $sent_password) === TRUE){
        try {
            $query = "SELECT * FROM users.users WHERE email = :email AND `password` = :password";
            $sql = $conn->prepare($query);
            $sql->bindParam('email', $sent_email, PDO::PARAM_STR);
            $sql->bindParam('password', $sent_password, PDO::PARAM_STR);
            $sql->execute();
            $count = $sql->rowCount();
            $row   = $sql->fetch(PDO::FETCH_ASSOC);
            // $row = $sql->fetchAll();
            // $row['email'] doesn't work with fetch all. 
            // if (count($row) == 1 && !empty($row)) {
            if ($count == 1 && !empty($row)) {
                $_SESSION['login']   = $row['email'];
                if (!empty($_SESSION['login'])){
                    header('Location: article.php');
                }else {
                    $msg =  "The login information did  not work. Please try again.";
                }
            }
            else {
                $msg =  "The login information is incorrect";
            }
        }catch (PDOException $e) {
            echo "Error : ".$e->getMessage();
        }
    }
};
?>